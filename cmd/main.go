package main

import (
	"time"

	"gitlab.com/tyronewilson/timing"
)

func main() {
	start := time.Now()
	finish := start.Add(time.Duration(24 * time.Hour))
	timeframe := timing.Timeframe{Start: start, Finish: finish}
	items := timing.HalfHourlyTimestamps(timeframe)

	frames := items.ToTimeframes()
	frames.PrettyPrint()
}
