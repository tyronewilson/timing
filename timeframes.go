package timing

import (
	"os"
	"time"

	"github.com/jedib0t/go-pretty/table"
)

// Timeframe represents a section of time with start and finish time
type Timeframe struct {
	Start  time.Time
	Finish time.Time
}

// TimeframeList is a slice of Timeframes which has additional functionality
// added via recievers
type TimeframeList []Timeframe

// PrettyPrint provides ASCII rendering of TimeframeList
func (tfl *TimeframeList) PrettyPrint() {
	t := table.NewWriter()
	t.SetOutputMirror(os.Stdout)
	t.AppendHeader(table.Row{"#", "START", "FINISH"})

	for i, item := range *tfl {
		t.AppendRow(item.toTableRow(i))
	}
	t.Render()
}

func (tf *Timeframe) toTableRow(index int) table.Row {
	return table.Row{index, tf.Start, tf.Finish}
}
