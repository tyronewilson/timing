package timing

import (
	"fmt"
	"time"

	"github.com/teambition/rrule-go"
)

// HalfHourlyRule returns a rrule.RRule which runs every 30 minutes
func HalfHourlyRule(start time.Time) *rrule.RRule {
	start = start.Round(time.Duration(30 * time.Minute))
	r, _ := rrule.NewRRule(rrule.ROption{
		Freq:     rrule.MINUTELY,
		Interval: 30,
		Dtstart:  start,
	})

	return r
}

// HalfHourlyTimestamps returns half hourly timestamps within the provided
// Timeframe
func HalfHourlyTimestamps(frame Timeframe) TimestampList {
	return HalfHourlyRule(frame.Start).Between(frame.Start, frame.Finish, true)
}

// TimestampList is a slice of timestamps
type TimestampList []time.Time

// PrettyPrint provides pretty format printing for a TimestampList
func (tl *TimestampList) PrettyPrint() {
	fmt.Println("[")

	for _, t := range *tl {
		fmt.Println(t)
	}
	fmt.Println("]")
}

// ToTimeframes returns list of timeframes between the timestamps in the
// TimestampList
func (tl TimestampList) ToTimeframes() TimeframeList {
	ret := TimeframeList{}

	startTimes := tl[0 : len(tl)-1]
	finishTimes := tl[1:len(tl)]

	for i, t := range startTimes {
		ret = append(ret, Timeframe{Start: t, Finish: finishTimes[i]})
	}
	return ret
}

// DailyRule returns an rrule.Rule which is set to every day
func DailyRule(start time.Time) *rrule.RRule {
	start = start.Truncate(time.Duration(24 * time.Hour))

	r, _ := rrule.NewRRule(rrule.ROption{
		Freq:     rrule.DAILY,
		Interval: 1,
		Dtstart:  start,
	})
	return r
}

// DailySchedule provides a schedule of daily timestamps for the timeframe
// provided.
func DailySchedule(tf Timeframe) TimestampList {
	return DailyRule(tf.Start).Between(tf.Start, tf.Finish, true)
}
